
provider "google" {
  credentials = "${file("/var/lib/jenkins/workspace/terraform-pipeline/jenkinsrepo/jenkins-sa.json")}"
  project     = "kavita-248719"
  region      = "us-central1"
  zone        = "us-central1-a"
}

resource "google_compute_instance" "vm_instance" {
  name         = "kavitavm"
  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config {
    }
  }
}
